//入口文件
import Vue from 'vue'
//1.1导入路由的包
import VueRouter from 'vue-router'
//1.2安装路由
Vue.use(VueRouter);
//1.3导入自己的router.js模块
import router from './router.js'

//注册vuex
import Vuex from 'vuex'
Vue.use(Vuex)

//每次进入网站，肯定会调用mian.js,在刚调用的时候，先从本地存储中把购物车的数据调出来，放到store中
var car = JSON.parse(localStorage.getItem('car' || '[]'))
var store = new Vuex.Store({
    state: { //this.$store.state.***
        car: car, //将购物车中的商品的数据，用一个数组储存起来，存储的是一些商品的对象
        //{id:商品的id,count:'要购买的数量'，price:商品的单价，selected:false}
    },
    mutations: { //this.$store.commit('方法名称'，'按需传递唯一的参数')
        addToCar(state, goodsinfo) {
            //点击加入购物车，把商品信息，保存到store中的car上
            //分析：
            //1.如果购物车中，之前就已经有这个对于的商品，那么只需要更新数量
            //2.如果没有，则直接把商品数据push到car中即可

            var flag = false; //假设没有找到商品
            state.car.some(item => {
                if (item.id == goodsinfo.id) {
                    item.count += parseInt(goodsinfo.count)
                    flag = true;
                    return true
                }
            });

            if (!flag) {
                state.car.push(goodsinfo)
            }

            //如果最后flag还是false，那么直接把goodsinfo push到购物车中
            localStorage.setItem('car', JSON.stringify(state.car));
            //当更新 car 之后，白 car 数组，存储到 localStoreage中
        },
        updateGoodsInfo(state, goodsinfo) {
            //修改购物车中商品的数量值
            state.car.some(item => {
                    if (item.id == goodsinfo.id) {
                        item.count = parseInt(goodsinfo.count)
                        return true
                    }
                })
                //当修改完商品的数量把最新的购物车数据保存到本地存储中
            localStorage.setItem('car', JSON.stringify(state.car));
        },
        removeFromCar(state, id) {
            //根据id从store中的购物车中删除对应的数据
            state.car.some((item, i) => {
                if (item.id == id) {
                    state.car.splice(i, 1);
                    return true
                }
            });
            //当删除 完毕之后，把 car 数组，存储到 localStoreage中
            localStorage.setItem('car', JSON.stringify(state.car));

        },
        updateGoodsSelected(state, info) {
            state.car.some(item => {
                if (item.id == info.id) {
                    item.selected = info.selected

                }
            });
            //当所有商品的状态把最新的购物车数据保存到本地存储中
            localStorage.setItem('car', JSON.stringify(state.car));
        }

    },
    getters: { //this.$store.getters.****
        //相当于计算属性也相当于filters
        getAllCount(state) {
            var c = 0;
            state.car.forEach(item => {
                c += item.count
            })
            return c
        },
        getGoodsCount(state) {
            var o = {};
            state.car.forEach(item => {
                o[item.id] = item.count;
            })
            return o
        },
        getGoodsSelected(state) {
            var o = {};
            state.car.forEach(item => {
                o[item.id] = item.selected;
            })
            return o
        },
        getGoodsCountAndAmount(state) {
            var o = {
                count: 0, //勾选的数量
                amount: 0, //勾选的总价
            };
            state.car.forEach(item => {
                if (item.selected) {
                    o.count += item.count;
                    o.amount += item.count * item.price
                }
            });
            return o
        }
    },
});

//2.1 导入vue-resource
import VueResource from 'vue-resource';
// 2.2 安装 vue-resource
Vue.use(VueResource);
//设置请求的根路径
Vue.http.options.root = 'http://www.liulongbin.top:3005/';
//全局设置 post时候表单数据组织格式
Vue.http.options.emulateJSON = true;

//导入时间插件
import moment from 'moment';
//定义全局过滤器
Vue.filter('dateFormat', function(dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {
    return moment(dataStr).format(pattern);
})

//导入App组件
import app from './App.vue'

//按需导入 Mint-ui 中的组件
/* import { Header, Swipe, SwipeItem, Button, Lazyload } from 'mint-ui';
Vue.component(Header.name, Header);
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);
Vue.use(Lazyload); */
import MintUI from 'mint-ui';
Vue.use(MintUI);
import 'mint-ui/lib/style.css';

//安装图片预览插件
import VuePreview from 'vue-preview'

// defalut install
Vue.use(VuePreview) // with parameters install
Vue.use(VuePreview, {
    mainClass: 'pswp--minimal--dark',
    barsSize: {
        top: 0,
        bottom: 0
    },
    captionEl: false,
    fullscreenEl: false,
    shareEl: false,
    bgOpacity: 0.85,
    tapToClose: true,
    tapToToggleControls: false
})


//导入MUI的样式
import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

var vm = new Vue({
    el: '#app',
    render: c => c(app),
    router, //挂在路由对象到vm实例上
    store //挂载store状态管理对象
})